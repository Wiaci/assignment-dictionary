%define first 0
%macro colon 2
%%megalabel:
    dq first
    db %1, 0
%2:
    %define first %%megalabel
%endmacro