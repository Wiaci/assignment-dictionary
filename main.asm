%define BUFFER_LENGTH 256

global _start

section .rodata

    %include "words.inc"

    read_error_msg: db "Bigger than buffer", 0
    no_such_key_msg: db "No such key", 0

section .text

extern find_word
extern string_length
extern print_string
extern print_err
extern read_word
extern exit

_start:
    sub rsp, BUFFER_LENGTH
    mov rdi, rsp
    mov rsi, BUFFER_LENGTH
    call read_word
    cmp rax, 0
    je .read_error
    mov rdi, rsp
    mov rsi, first
    call find_word
    cmp rax, 0
    je .no_such_key
    add rax, 8
    push rax
    mov rdi, rax
    call string_length
    inc rax
    mov rsi, rax
    pop rdi
    add rdi, rsi
    call print_string
    jmp .end
    .read_error:
        mov rdi, read_error_msg
        call print_err
        jmp .err_end
    .no_such_key:
        mov rdi, no_such_key_msg
        call print_err
    .err_end:
        mov rdi, 1
    .end:
        call exit
