prog: lib.o dict.o main.o
	ld -o prog lib.o dict.o main.o

lib.o: lib.asm
	nasm -f elf64 -o lib.o lib.asm

dict.o: dict.asm
	nasm -f elf64 -o dict.o dict.asm

main.o: main.asm words.inc colon.inc
	nasm -f elf64 -o main.o main.asm

clean:
	rm *.o prog