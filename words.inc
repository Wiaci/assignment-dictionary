%include "colon.inc"

colon "let_key", w1
db "let", 0

colon "me_key", w2
db "me", 0

colon "take_key", w3
db "take", 0

colon "you_key", w4
db "you", 0

colon "down_key", w5
db "down", 0

colon "cause_key", w6
db "cause", 0

colon "i'm_key", w7
db "i'm", 0

colon "going_key", w8
db "going", 0

colon "to_key", w9
db "to", 0

colon "strawberry_key", w10
db "strawberry", 0

colon "fields_key", w11
db "fields", 0

colon "nothing_key", w12
db "nothing", 0

colon "is_key", w13
db "is", 0

colon "real_key", w14
db "real", 0

colon "and_key", w15
db "and", 0

colon "get_key", w16
db "get", 0

colon "hung_key", w17
db "hung", 0

colon "about_key", w18
db "about", 0

colon "forever_key", w19
db "forever", 0
