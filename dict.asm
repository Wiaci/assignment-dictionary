section .text

global find_word

extern string_equals

# rdi - string pointer
# rsi - dictionary start pointer
find_word:
    push r9
    .loop:
        mov r9, [rsi]
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        cmp rax, 1
        je .found
        cmp r9, 0
        je .not_found
        mov rsi, r9
        jmp .loop
        .found:
            mov rax, rsi
            jmp .end
        .not_found:
            xor rax, rax
        .end:
            pop r9
            ret